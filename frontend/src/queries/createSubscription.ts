import { gql } from '@apollo/client';
  
const CREATE_SUBSCRIPTION = gql`
  mutation createSubscription($subscription: SubscriptionInput!) {
    createSubscription(subscription: $subscription) {
      id
      endsAt
    }
  }
`;

export default CREATE_SUBSCRIPTION;