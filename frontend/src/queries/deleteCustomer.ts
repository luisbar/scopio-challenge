import { gql } from '@apollo/client';
  
const DELETE_CUSTOMER = gql`
  mutation deleteCustomer($id: String!) {
    deleteCustomer(id: $id) {
      id
    }
  }
`;

export default DELETE_CUSTOMER;