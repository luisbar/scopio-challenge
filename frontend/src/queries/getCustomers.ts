import { gql } from '@apollo/client';
  
const GET_CUSTOMERS = gql`
  query findCustomers($filter: String!) {
    findCustomers(filter: $filter) {
      id
      firstName
      lastName
      role
      email
      subscription {
        id
        endsAt
        plan {
          id
          name
        }
        paymentGateway {
          id
          name
        }
      }
    }
  }
`;

export default GET_CUSTOMERS;