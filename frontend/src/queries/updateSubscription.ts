import { gql } from '@apollo/client';
  
const UPDATE_SUBSCRIPTION = gql`
  mutation updateSubscription($id: String!, $subscription: SubscriptionInput!) {
    updateSubscription(id: $id, subscription: $subscription) {
      id
      endsAt
      customer {
        firstName
      }
    }
  }
`;

export default UPDATE_SUBSCRIPTION;