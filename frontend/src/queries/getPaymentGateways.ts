import { gql } from '@apollo/client';

const GET_PAYMENT_GATEWAYS = gql`
  query {
    findPaymentGateways {
      id
      name
    }
  }
`;

export default GET_PAYMENT_GATEWAYS;