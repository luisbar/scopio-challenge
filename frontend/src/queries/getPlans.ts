import { gql } from '@apollo/client';

const GET_PLANS = gql`
  query {
    findPlans {
      id
      name
    }
  }
`;

export default GET_PLANS;