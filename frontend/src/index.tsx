import * as React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import App from './components/pages/App';
import theme from './theme';
import { ApolloProvider } from "@apollo/client";
import client from "./utils/client";

ReactDOM.render(
  <ThemeProvider theme={theme}>
    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
    <CssBaseline />
      <ApolloProvider client={client}>
        <App />
      </ApolloProvider>
  </ThemeProvider>,
  document.querySelector('#root'),
);
