import React, { useState, useEffect } from 'react';
import { TextField, Grid, Button, } from '@mui/material';
import { useQuery } from '@apollo/client';
import GET_PLANS from '../../queries/getPlans';
import GET_PAYMENT_GATEWAYS from '../../queries/getPaymentGateways';
import * as ramda from 'ramda';
import DebouncedDropdown from '../atoms/DebouncedDropdown';
import GET_CUSTOMERS from '../../queries/getCustomers';
import UPDATE_SUBSCRIPTION from '../../queries/updateSubscription';
import CREATE_SUBSCRIPTION from '../../queries/createSubscription';
import DELETE_CUSTOMER from '../../queries/deleteCustomer';
import client from '../../utils/client';
import Dropdown from '../atoms/Dropdown';
import Spinner from '../atoms/Spinner';
import Toast from '../molecules/Toast';

const initialState = {
  id: '',
  firstName: '',
  lastName: '',
  email: '',
  role: '',
  subscription: {
    id: '',
    endsAt: '',
    plan: {
      id: '',
    },
    paymentGateway: {
      id: '',
    },
  }
};

const Form = () => {
  const [customer, setCustomer] = useState(initialState);
  const plansResponse = useQuery(GET_PLANS);
  const paymentGatewwaysResponse = useQuery(GET_PAYMENT_GATEWAYS);
  const plans = ramda.pathOr([], ['data', 'findPlans'], plansResponse);
  const paymentGateways = ramda.pathOr([], ['data', 'findPaymentGateways'], paymentGatewwaysResponse);
  const [loading, setLoading] = useState(false);
  const defaultError: any = {};
  const [error, setError] = useState(defaultError);

  useEffect(() => {
    setLoading(ramda.pathOr(false, ['loading'], plansResponse) || ramda.pathOr(false, ['loading'], paymentGatewwaysResponse));
  }, [plansResponse.loading, paymentGatewwaysResponse.loading]);

  useEffect(() => {
    setError(ramda.pathOr({}, ['error'], plansResponse) || ramda.pathOr({}, ['error'], paymentGatewwaysResponse));
  }, [plansResponse.error, paymentGatewwaysResponse.error]);
  
  const onSelect = (customer: Customer) => {
    if (!customer) return setCustomer(initialState);
    setCustomer(customer);
  }

  const onChange = (event: any) => {
    setCustomer((currentCustomer): any => {
      const path = (event.target.name || event.target.id).split('.');
      
      return ramda.assocPath(path, event.target.value, currentCustomer);
    });
  }

  const updateSubscription = async () => {
    try {
      setLoading(true);
      await client.mutate({
        mutation: UPDATE_SUBSCRIPTION,
        variables: {
          id: customer.subscription.id,
          subscription: {
            endsAt: customer.subscription.endsAt,
            customer: {
              id: customer.id,
              firstName: customer.firstName,
              lastName: customer.lastName,
              email: customer.email,
              role: customer.role,
            },
            planId: customer.subscription.plan.id,
            paymentGatewayId: customer.subscription.paymentGateway.id,
          }
        }
      });
      setLoading(false);
      setError({});
    } catch (error: any) {
      setLoading(false);
      setError({ message: error.networkError.result.errors[0].message, fields: mapFieldErrors(error.networkError.result.errors[0].fields) });
    }
  }

  const createSubscription = async () => {
    try {
      setLoading(true);
      const { errors } = await client.mutate({
        mutation: CREATE_SUBSCRIPTION,
        variables: {
          subscription: {
            endsAt: new Date(),
            customer: {
              firstName: customer.firstName,
              lastName: customer.lastName,
              email: customer.email,
              role: customer.role,
            },
            planId: customer.subscription.plan.id,
            paymentGatewayId: customer.subscription.paymentGateway.id,
          }
        }
      });
      setLoading(false);
      setError({});
    } catch (error: any) {
      setLoading(false);
      setError({ message: error.networkError.result.errors[0].message, fields: mapFieldErrors(error.networkError.result.errors[0].fields) });
    }
  }

  const deleteCustomer = async () => {
    setLoading(true);
    const { errors } = await client.mutate({
      mutation: DELETE_CUSTOMER,
      variables: {
        id: customer.id,
      }
    });
    setLoading(false);
    if (!errors?.length) setCustomer(initialState);
    if (errors?.length) setError(errors[0]);
  }

  const mapFieldErrors = (fieldErrors: any) => {
    return ramda.compose(
      ramda.fromPairs,
      ramda.map((item): any => [item[0].split('.').pop(), item[1]]),
      ramda.toPairs,
    )(fieldErrors)
  }

  return (
    <form
      data-testid="form"
    >
      <Toast
        message={error.message}
        visible={ramda.pathOr(false, ['message'], error)}
      />
      <Grid
        container
        spacing={2}
      >
        <Grid
          item
          xs={12}
        >
          <DebouncedDropdown
            query={GET_CUSTOMERS}
            onSelect={onSelect}
            fieldsToShow={['firstName', 'lastName']}
          />
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
        >
          <TextField
            fullWidth
            id="firstName"
            label="First name"
            variant="outlined"
            value={customer.firstName}
            onChange={onChange}
            error={ramda.pathOr(false, ['fields', 'firstName'], error)}
            helperText={error?.fields?.firstName}
          />
        </Grid>
        <Grid
          item
          xs={12}
          md={6}
        >
          <TextField
            fullWidth
            id="lastName"
            label="Last name"
            variant="outlined"
            value={customer.lastName}
            onChange={onChange}
            error={ramda.pathOr(false, ['fields', 'lastName'], error)}
            helperText={error?.fields?.lastName}
          />
        </Grid>
        <Grid
          item
          xs={12}
        >
          <TextField
            fullWidth
            id="role"
            label="Role"
            variant="outlined"
            value={customer.role}
            onChange={onChange}
            error={ramda.pathOr(false, ['fields', 'role'], error)}
            helperText={error?.fields?.role}
          />
        </Grid>
        <Grid
          item
          xs={12}
        >
          <TextField
            fullWidth
            id="email"
            label="Email"
            variant="outlined"
            value={customer.email}
            onChange={onChange}
            error={ramda.pathOr(false, ['fields', 'email'], error)}
            helperText={error?.fields?.email}
          />
        </Grid>
        <Grid
          item
          xs={12}
        >
          <Dropdown
            items={plans}
            value={customer?.subscription?.plan?.id || ''}
            labelId="subscriptionPlanLabel"
            label="Subscription plan"
            selectId="subscription.plan.id"
            onChange={onChange}
            error={ramda.pathOr(false, ['fields', 'planId'], error)}
            errorMessage={error?.fields?.planId}
          />
        </Grid>
        <Grid
          item
          xs={12}
        >
          <Dropdown
            items={paymentGateways}
            value={customer?.subscription?.paymentGateway?.id || ''}
            labelId="subscriptionPaymentGatewayLabel"
            label="Payment gateway"
            selectId="subscription.paymentGateway.id"
            onChange={onChange}
            error={ramda.pathOr(false, ['fields', 'paymentGatewayId'], error)}
            errorMessage={error?.fields?.paymentGatewayId}
          />
        </Grid>
        <Grid
          item
          xs={12}
        >
          {
            !customer.id &&
            <Button
              onClick={createSubscription}
              variant="contained"
            >
              Add Customer
            </Button>
          }
          {
            customer.id &&
            <Button
              onClick={updateSubscription}
              variant="contained"
            >
              Update Customer
            </Button>
          }
          {
            customer.id &&
            <Button
              onClick={deleteCustomer}
              variant="outlined"
              sx={{
                marginLeft: 2
              }}
            >
              Delete Customer
            </Button>
          }
        </Grid>
      </Grid>
      <Spinner
        visible={loading}
      />
    </form>
  );
}

interface Plan {
  id: string;
  name: string;
}

interface PaymentGateway {
  id: string;
  name: string;
}

interface Customer {
  id: string;
  firstName: string;
  lastName: string;
  role: string;
  email: string;
  subscription: Subscription;
}

interface Subscription {
  id: string;
  endsAt: string;
  plan: Plan;
  paymentGateway: PaymentGateway;
}

interface CustomError {
  error: any;
}

export default Form;