import React, { useState } from 'react';
import { Autocomplete, TextField } from '@mui/material';
import client from '../../utils/client';
import { DocumentNode } from '@apollo/client';
let timeout: any = null;

const DebouncedDropdown = ({ query, fieldsToShow, onSelect }: IProps) => {
  const [options, setOptions] = useState([]);

  const onInputChange = (event: any, value: String) => {
    if (!value) return;
    if (timeout) clearTimeout(timeout);
    
    timeout = setTimeout(async () => {
      const response = await client.query({
        query,
        variables: {
          filter: value.trim()
        },
      });

      setOptions(response.data.findCustomers || []);
    }
    , 1000);
  };

  const onChange = (event: any, value: any) => {
    if (!value) return onSelect(null);
    onSelect(value);
  }

  return (
    <Autocomplete
      fullWidth
      options={options}
      onChange={onChange}
      getOptionLabel={(option: any) => fieldsToShow.reduce((acc, field) => `${acc} ${option[field]}`, '')}
      onInputChange={onInputChange}
      renderInput={(params) => (
        <TextField {...params} label="Search" />
      )}
    />
  );
}

interface IProps {
  query: DocumentNode;
  onSelect: CallableFunction;
  fieldsToShow: string[];
}

export default DebouncedDropdown;