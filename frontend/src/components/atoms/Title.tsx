import React from 'react';
import { Typography } from '@mui/material';

const Title = () => {
  return (
    <Typography
      data-testid="title"
      variant="h5"
      color="text.secondary"
      align="center"
      margin={2}
    >
      Customer Management
    </Typography>
  );
}

export default Title;