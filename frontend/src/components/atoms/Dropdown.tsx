import React from "react";
import { FormControl, InputLabel, Select, MenuItem, FormHelperText } from "@mui/material";

const Dropdown = ({ value, items, label, labelId, selectId, onChange, error, errorMessage }: IProps) => {
  return (
    <FormControl
      fullWidth
      error={error}
    >
      <InputLabel id={labelId}>{label}</InputLabel>
      <Select
        labelId={labelId}
        id={selectId}
        label={label}
        name={selectId}
        variant="outlined"
        value={value}
        onChange={onChange}
      >
        {items.map((item: Item) => (
          <MenuItem
            key={item.id}
            value={item.id}
          >
            {item.name}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>{errorMessage}</FormHelperText>
    </FormControl>
  );
}

interface IProps {
  value: string;
  items: Item[];
  label: string;
  labelId: string;
  selectId: string;
  onChange: any;
  error: boolean;
  errorMessage: string;
}

interface Item {
  id: string;
  name: string;
}

export default Dropdown;