import React from 'react';
import { CircularProgress, Box } from '@mui/material';

const Spinner = ({ visible }: IProps) => {
  if (!visible) return null;

  return (
    <Box
      sx={{
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        backdropFilter: 'blur(5px)'
      }}
    >
      <CircularProgress
        color='primary'
      />
    </Box>
  );
};

interface IProps {
  visible: boolean;
}

export default Spinner;