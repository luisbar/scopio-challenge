import React from 'react';
import { Container, Box } from '@mui/material';

const Centered = ({ children }: IProps) => {
  return (
    <Box
      data-testid="centered"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
      }}
    >
      <Container
        maxWidth="sm"
      >
        {children}
      </Container>
    </Box>
  );
}

interface IProps {
  children: React.ReactNode;
}

export default Centered;

