import * as React from 'react';
import Form from '../organisms/Form';
import Title from '../atoms/Title';
import Centered from '../templates/Centered';

const App = () => {
  return (
    <Centered>
      <Title />
      <Form />
    </Centered>
  );
}

export default App;
