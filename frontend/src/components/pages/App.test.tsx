import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import theme from '../../theme';
import { ApolloProvider } from "@apollo/client";
import client from "../../utils/client";

beforeEach(() => {
  render(
    <ThemeProvider theme={theme}>
      {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
      <CssBaseline />
        <ApolloProvider client={client}>
          <App />
        </ApolloProvider>
    </ThemeProvider>
  );
});

test('renders <Centered> component', () => {
  const centered = screen.getByTestId(/centered/i);
  expect(centered).toBeInTheDocument();
});

test('renders <Title> component', () => {
  const title = screen.getByTestId(/title/i);
  expect(title).toBeInTheDocument();
});

test('renders <Form> component', () => {
  const form = screen.getByTestId(/form/i);
  expect(form).toBeInTheDocument();
});