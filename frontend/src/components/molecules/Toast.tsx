import React from 'react';
import { Stack, Alert, AlertTitle } from '@mui/material';

const Toast = ({ visible, message }: IProps) => {
  if (!visible) return null;

  return (
      <Stack
        sx={{
          position: 'fixed',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          display: 'flex',
          alignItems: 'center',
        }} spacing={2}
      >
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        {message}
      </Alert>
    </Stack>
  );
}

interface IProps {
  visible: boolean;
  message: string;
}

export default Toast;