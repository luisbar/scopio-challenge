import { Mutation, Resolver, Query, Args } from '@nestjs/graphql';
import { LoggerService } from '../common/logger/logger.service';
import { Customer } from './customer.model';
import { CustomersService } from './customers.service';
import { CustomerInput } from './dtos/customer.input';
import { CustomerOutput } from './dtos/customer.output';

@Resolver(of => CustomerOutput)
export class CustomersResolver {
  constructor(
    private customersService: CustomersService,
    private readonly logger: LoggerService,
  ) {
    this.logger.setContext(CustomersResolver.name);
  }

  @Query(returns => [CustomerOutput])
  findCustomers(
    @Args('filter') filter: string,
  ) {
    try {
      return this.customersService.findAll(filter);
    } catch (error) {
      throw this.logger.error(
        'Cannot get customers',
        error,
      );
    }
  }

  @Query(returns => CustomerOutput)
  findCustomer(
    @Args('id') id: string,
  ) {
    try {
      return this.customersService.findOne(id);
    } catch (error) {
      throw this.logger.error(
        'Cannot get the customer',
        error,
      );
    }
  }

  @Mutation(returns => CustomerOutput)
  createCustomer(
    @Args('customer', { type: () => CustomerInput }) customer: CustomerInput,
  ) {
    try {
      return this.customersService.create(customer as Customer);
    } catch (error) {
      throw this.logger.error(
        'Cannot create the customer',
        error,
      );
    }
  }

  @Mutation(returns => CustomerOutput)
  updateCustomer(
    @Args('id') id: string,
    @Args('customer', { type: () => CustomerInput }) customer: CustomerInput,
  ) {
    try {
      return this.customersService.update(id, customer as Customer);
    } catch (error) {
      throw this.logger.error(
        'Cannot update the customer',
        error,
      );
    }
  }

  @Mutation(returns => CustomerOutput)
  deleteCustomer(
    @Args('id') id: string,
  ) {
    try {
      return this.customersService.delete(id);
    } catch (error) {
      throw this.logger.error(
        'Cannot delete the customer',
        error,
      );
    }
  }
}