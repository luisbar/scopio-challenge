import { Test, TestingModule } from '@nestjs/testing';
import { CustomersService } from './customers.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { Customer } from './customer.model';
import { ConfigModule } from '@nestjs/config';
import { Subscription } from '../subscriptions/subscription.model';
import { Plan } from '../plans/plan.model';
import { PaymentGateway } from '../paymentGateways/paymentGateway.model';
let customerId;

describe('CustomersService', () => {
  let service: CustomersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        SequelizeModule.forRootAsync({
          useFactory: () => ({
            dialect: 'postgres',
            host: process.env.DATABASE_HOST,
            port: Number(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
            synchronize: process.env.NODE_ENV !== 'production',
            autoLoadModels: process.env.NODE_ENV !== 'production',
            models: [Customer, Subscription, Plan, PaymentGateway],
          })
        }),
        SequelizeModule.forFeature([Customer]),
        ConfigModule.forRoot({
          ignoreEnvFile: Boolean(process.env.IGNORE_ENV_FILE),
          isGlobal: true,
        }),
      ],
      providers: [
        CustomersService
      ],
    }).compile();

    service = module.get<CustomersService>(CustomersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a customer', async () => {
    const customer = await service.create({
      firstName: 'John',
      lastName: 'Doe',
      role: 'admin',
      email: 'admin@mail.com',
    } as Customer);
    customerId = customer.id;

    expect(customer.toJSON()).toMatchObject({
      firstName: 'John',
      lastName: 'Doe',
      role: 'admin',
      email: 'admin@mail.com',
    });
  });

  it('should find a customer by id', async () => {
    const customer = await service.findOne(customerId);

    expect(customer.toJSON()).toMatchObject({
      firstName: 'John',
      lastName: 'Doe',
      role: 'admin',
      email: 'admin@mail.com',
    });
  });

  it('should get all customers', async () => {
    const customers = await service.findAll(null);
    expect(customers.length).toBeGreaterThan(0);
  });

  it('should update a customer', async () => {
    const customer = await service.update(customerId, {
      firstName: 'Jane',
      lastName: 'Doe',
      role: 'user',
      email: 'user@mail.com',
    } as Customer);
    
    expect(customer.toJSON()).toMatchObject({
      firstName: 'Jane',
      lastName: 'Doe',
      role: 'user',
      email: 'user@mail.com',
    });
  });

  it('should delete a customer', async () => {
    const customer = await service.delete(customerId);

    expect(customer).toMatchObject({
      firstName: 'Jane',
      lastName: 'Doe',
      role: 'user',
      email: 'user@mail.com',
    });
  });
});
