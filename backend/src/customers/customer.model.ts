import { Column, Model, Table, HasOne, CreatedAt, UpdatedAt, DeletedAt } from 'sequelize-typescript';
import { DataTypes } from 'sequelize';
import { Subscription } from '../subscriptions/subscription.model';

@Table
export class Customer extends Model {
  @Column({
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  })
  id: string;

  @Column
  firstName: string;

  @Column
  lastName: string;

  @Column
  role: string;

  @Column
  email: string;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @DeletedAt
  deletedAt: Date;

  @HasOne(() => Subscription)
  subscription: Subscription
}