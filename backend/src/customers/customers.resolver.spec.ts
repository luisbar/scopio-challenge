import { Test, TestingModule } from '@nestjs/testing';
import { CustomersResolver } from './customers.resolver';
import { CustomersService } from './customers.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { Customer } from './customer.model';
import { ConfigModule } from '@nestjs/config';
import { Subscription } from '../subscriptions/subscription.model';
import { Plan } from '../plans/plan.model';
import { PaymentGateway } from '../paymentGateways/paymentGateway.model';
import { CommonModule } from '../common/common.module';
let customerId;

describe('CustomersResolver', () => {
  let resolver: CustomersResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        SequelizeModule.forRootAsync({
          useFactory: () => ({
            dialect: 'postgres',
            host: process.env.DATABASE_HOST,
            port: Number(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
            synchronize: process.env.NODE_ENV !== 'production',
            autoLoadModels: process.env.NODE_ENV !== 'production',
            models: [Customer, Subscription, Plan, PaymentGateway],
          })
        }),
        SequelizeModule.forFeature([Customer]),
        ConfigModule.forRoot({
          ignoreEnvFile: Boolean(process.env.IGNORE_ENV_FILE),
          isGlobal: true,
        }),
        CommonModule,
      ],
      providers: [CustomersResolver, CustomersService],
    }).compile();

    resolver = module.get<CustomersResolver>(CustomersResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('should add a customer', async () => {
    const customer = await resolver.createCustomer({
      firstName: 'John',
      lastName: 'Doe',
      role: 'admin',
      email: 'admin@mail.com',
    });
    customerId = customer.id;

    expect(customer).toMatchObject({
      firstName: 'John',
      lastName: 'Doe',
      role: 'admin',
      email: 'admin@mail.com',
    });
  });

  it('should find a customer', async () => {
    const customer = await resolver.findCustomer(customerId);

    expect(customer).toMatchObject({
      firstName: 'John',
      lastName: 'Doe',
      role: 'admin',
      email: 'admin@mail.com',
    });
  });

  it('should return an array of customers', async () => {
    const customers = await resolver.findCustomers(null);
    expect(customers.length).toBeGreaterThan(0);
  });

  it('should update a customer', async () => {
    const customer = await resolver.updateCustomer(customerId, {
      firstName: 'Jane',
      lastName: 'Doe',
      role: 'user',
      email: 'user@mail.com',
    });

    expect(customer).toMatchObject({
      firstName: 'Jane',
      lastName: 'Doe',
      role: 'user',
      email: 'user@mail.com',
    });
  });

  it('should delete a customer', async () => {
    const customer = await resolver.deleteCustomer(customerId);

    expect(customer).toMatchObject({
      firstName: 'Jane',
      lastName: 'Doe',
      role: 'user',
      email: 'user@mail.com',
    });
  });
});
