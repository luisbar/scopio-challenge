import { Module } from '@nestjs/common';
import { Customer } from './customer.model';
import { CustomersService } from './customers.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { CustomersResolver } from './customers.resolver';
import { CommonModule } from 'src/common/common.module';

@Module({
  imports: [SequelizeModule.forFeature([Customer]), CommonModule],
  providers: [CustomersService, CustomersResolver],
})
export class CustomersModule {}
