import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { PaymentGateway } from '../paymentGateways/paymentGateway.model';
import { Plan } from '../plans/plan.model';
import { Subscription } from '../subscriptions/subscription.model';
import { Customer } from './customer.model';
import { Op } from 'sequelize';

@Injectable()
export class CustomersService {
  constructor(
    @InjectModel(Customer)
    private customerModel: typeof Customer,
  ) {}

  findAll(
    filter: String,
  ): Promise<Customer[]> {
    let query = {
      where: {},
      include: [
        {
          model: Subscription,
          as: 'subscription',
          include: [
            {
              model: Plan,
              as: 'plan',
            },
            {
              model: PaymentGateway,
              as: 'paymentGateway',
            },
          ],
        },
      ],
    }

    if (filter) query = {
      ...query,
      where: {
        [Op.or]: [
          ...filter.split(' ').map(word => ({
            firstName: {
              [Op.substring]: word,
            },
          })),
          ...filter.split(' ').map(word => ({
            lastName: {
              [Op.substring]: word,
            },
          })),
        ]
      },
    }

    return this.customerModel.findAll(query);
  }

  findOne(id: string): Promise<Customer> {
    return this.customerModel.findByPk(id, {
      include: [
        {
          model: Subscription,
          as: 'subscription',
          include: [
            {
              model: Plan,
              as: 'plan',
            },
            {
              model: PaymentGateway,
              as: 'paymentGateway',
            },
          ],
        },
      ],
    });
  }

  create(customer: Customer): Promise<Customer> {
    const { firstName, lastName, role, email, } = customer;
    return this.customerModel.create({
      firstName,
      lastName,
      role,
      email,
    });
  }

  async update(id: string, customer: Customer): Promise<Customer> {
    const { firstName, lastName, role, email, } = customer;
    await this.customerModel.update({
      firstName,
      lastName,
      role,
      email,
    }, { where: { id } });

    return this.customerModel.findByPk(id)
  }

  async delete(id: string): Promise<any> {
    const customerToDelete = (await this.customerModel.findByPk(id)).toJSON();
    await this.customerModel.destroy({ where: { id } });

    return customerToDelete;
  }
}
