import { Field, InputType } from '@nestjs/graphql';
import { IsString, Matches, IsEmail, IsNotEmpty } from 'class-validator';

@InputType()
export class CustomerInput {
    @Field(() => String, { nullable: true })
    id?: string;

    @Field(() => String, { nullable: false })
    @IsString({ message: 'Firstname is not a valid string' })
    @IsNotEmpty({ message: 'Firstname is not a valid string' })
    @Matches(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/, {
        message: 'Firstname does not match the required format',
    })
    firstName: string;

    @Field(() => String, { nullable: false })
    @IsString({ message: 'Lastname is not a valid string' })
    @IsNotEmpty({ message: 'Lastname is not a valid string' })
    @Matches(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/, {
        message: 'Lastname does not match the required format',
    })
    lastName: string;

    @Field(() => String, { nullable: false })
    @IsString({ message: 'Role is not a valid string' })
    @IsNotEmpty({ message: 'Role is not a valid string' })
    @Matches(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/, {
        message: 'Role does not match the required format',
    })
    role: string;

    @Field(() => String, { nullable: false })
    @IsString({ message: 'Email is not a valid string' })
    @IsEmail({}, { message: 'Email is not a valid email' })
    email: string;
}
