import { ObjectType, Field } from '@nestjs/graphql';
import { SubscriptionOutput } from '../../subscriptions/dtos/subscription.output';

@ObjectType()
export class CustomerOutput {
	@Field(() => String, { nullable:false })
	id: string;

	@Field(() => String, { nullable:false })
	firstName: string;
	
	@Field(() => String, { nullable:false })
  lastName: string;

	@Field(() => String, { nullable:false })
	role: string;
	
	@Field(() => String, { nullable:false })
	email: string;

	@Field(() => Date, { nullable:false })
	createdAt: Date;

	@Field(() => Date, { nullable:false })
	updatedAt: Date;

	@Field(() => Date, { nullable:true })
	deletedAt: Date;

	@Field(() => SubscriptionOutput, { nullable:true })
  subscription?: SubscriptionOutput;
}
