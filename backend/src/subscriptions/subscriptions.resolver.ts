import { Mutation, Resolver, Query, Args } from '@nestjs/graphql';
import { Subscription } from './subscription.model';
import { SubscriptionsService } from './subscriptions.service';
import { SubscriptionInput } from './dtos/subscription.input';
import { SubscriptionOutput } from './dtos/subscription.output';
import { LoggerService } from '../common/logger/logger.service';

@Resolver(of => SubscriptionOutput)
export class SubscriptionsResolver {
  constructor(
    private subscriptionsService: SubscriptionsService,
    private readonly logger: LoggerService,
  ) {
    this.logger.setContext(SubscriptionsResolver.name);
  }

  @Query(returns => [SubscriptionOutput])
  async findSubscriptions(
  ) {
    try {
      return this.subscriptionsService.findAll();
    } catch (error) {
      throw this.logger.error(
        'Cannot get subscriptions',
        error,
      );
    }
  }

  @Query(returns => SubscriptionOutput)
  findSubscription(
    @Args('id') id: string,
  ) {
    try {
      return this.subscriptionsService.findOne(id);
    } catch (error) {
      throw this.logger.error(
        'Cannot get the subscription',
        error,
      );
    }
  }

  @Mutation(returns => SubscriptionOutput)
  createSubscription(
    @Args('subscription', { type: () => SubscriptionInput }) subscription: SubscriptionInput,
  ) {
    try {
      return this.subscriptionsService.create(subscription as Subscription);
    } catch (error) {
      throw this.logger.error(
        'Cannot create the subscription',
        error,
      );
    }
  }

  @Mutation(returns => SubscriptionOutput)
  updateSubscription(
    @Args('id') id: string,
    @Args('subscription', { type: () => SubscriptionInput }) subscription: SubscriptionInput,
  ) {
    try {
      return this.subscriptionsService.update(id, subscription as Subscription);
    } catch (error) {
      throw this.logger.error(
        'Cannot update the subscription',
        error,
      );
    }
  }

  @Mutation(returns => SubscriptionOutput)
  deleteSubscription(
    @Args('id') id: string,
  ) {
    try {
      return this.subscriptionsService.delete(id);
    } catch (error) {
      throw this.logger.error(
        'Cannot delete the subscription',
        error,
      );
    }
  }
}