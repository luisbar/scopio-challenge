import { Column, Model, Table, CreatedAt, UpdatedAt, DeletedAt, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { DataTypes } from 'sequelize';
import { Customer } from '../customers/customer.model';
import { Plan } from '../plans/plan.model';
import { PaymentGateway } from '../paymentGateways/paymentGateway.model';

@Table
export class Subscription extends Model {
  @Column({
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  })
  id: string;

  @Column
  endsAt: Date;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @DeletedAt
  deletedAt: Date;

  @ForeignKey(() => Customer)
  customerId: string;

  @BelongsTo(() => Customer)
  customer: Customer

  @ForeignKey(() => Plan)
  planId: string;

  @BelongsTo(() => Plan)
  plan: Plan

  @ForeignKey(() => PaymentGateway)
  paymentGatewayId: string;

  @BelongsTo(() => PaymentGateway)
  paymentGateway: PaymentGateway
}