import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Customer } from '../customers/customer.model';
import { PaymentGateway } from '../paymentGateways/paymentGateway.model';
import { Plan } from '../plans/plan.model';
import { Subscription } from './subscription.model';

@Injectable()
export class SubscriptionsService {
  constructor(
    @InjectModel(Subscription)
    private subscriptionModel: typeof Subscription,
    @InjectModel(Customer)
    private customerModel: typeof Customer,
  ) {}

  findAll(): Promise<Subscription[]> {
    return this.subscriptionModel.findAll({
      include: [
        {
          model: Customer,
          as: 'customer',
        },
        {
          model: Plan,
          as: 'plan',
        },
        {
          model: PaymentGateway,
          as: 'paymentGateway',
        }
      ],
    });
  }

  findOne(id: string): Promise<Subscription> {
    return this.subscriptionModel.findByPk(id, {
      include: [
        {
          model: Customer,
          as: 'customer',
        },
        {
          model: Plan,
          as: 'plan',
        },
        {
          model: PaymentGateway,
          as: 'paymentGateway',
        }
      ],
    });
  }

  create(subscription: Subscription): Promise<Subscription> {
    const { endsAt, customer, planId, paymentGatewayId } = subscription;
    return this.subscriptionModel.create({
      endsAt,
      customer,
      planId,
      paymentGatewayId,
    }, {
      include: [
        {
          model: Customer,
          as: 'customer',
        },
        {
          model: Plan,
          as: 'plan',
        },
        {
          model: PaymentGateway,
          as: 'paymentGateway',
        }
      ],
    });
  }

  async update(id: string, subscription: Subscription): Promise<Subscription> {
    const { endsAt, customer, planId, paymentGatewayId } = subscription;
    const customerToUpdate = await this.customerModel.findByPk(customer.id);
    
    customerToUpdate.firstName = customer.firstName;
    customerToUpdate.lastName = customer.lastName;
    customerToUpdate.role = customer.role;
    customerToUpdate.email = customer.email;

    await customerToUpdate.save();

    await this.subscriptionModel.update({
      endsAt,
      planId,
      paymentGatewayId,
    }, {
      where: { id },
    });

    return this.subscriptionModel.findByPk(id, {
      include: [
        {
          model: Customer,
          as: 'customer',
        },
        {
          model: Plan,
          as: 'plan',
        },
        {
          model: PaymentGateway,
          as: 'paymentGateway',
        }
      ],
    })
  }

  async delete(id: string): Promise<any> {
    const subscriptionToDelete = (await this.subscriptionModel.findByPk(id, {
      include: [
        {
          model: Customer,
          as: 'customer',
        },
        {
          model: Plan,
          as: 'plan',
        },
        {
          model: PaymentGateway,
          as: 'paymentGateway',
        }
      ],
    })).toJSON();
    await this.subscriptionModel.destroy({ where: { id } });

    return subscriptionToDelete;
  }
}
