import { Field, InputType } from '@nestjs/graphql';
import { IsDate, IsUUID, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CustomerInput } from '../../customers/dtos/customer.input';

@InputType()
export class SubscriptionInput {
    @Field(() => Date, { nullable: false })
    @IsDate({ message: 'EndsAt is not a valid date' })
    endsAt: Date;

    @Field(() => CustomerInput, { nullable: false })
    @Type(() => CustomerInput)
    @ValidateNested({ each: true, message: 'There is something wrong with customer data' })
    customer?: CustomerInput;

    @Field(() => String, { nullable: false })
    @IsUUID('4', { message: 'PlanId is not a valid uuid' })
    planId: string;
    
    @Field(() => String, { nullable: false })
    @IsUUID('4', { message: 'PaymentGatewayId is not a valid uuid' })
    paymentGatewayId: string;
}
