import { ObjectType, Field } from '@nestjs/graphql';
import { CustomerOutput } from '../../customers/dtos/customer.output';
import { PaymentGatewayOutput } from '../../paymentGateways/dtos/paymentGateway.output';
import { PlanOutput } from '../../plans/dtos/plan.output';

@ObjectType()
export class SubscriptionOutput {
	@Field(() => String, { nullable:false })
	id: string;

	@Field(() => Date, { nullable:false })
	endsAt: Date;
	
	@Field(() => CustomerOutput, { nullable:false })
  customer: CustomerOutput;
	
	@Field(() => PlanOutput, { nullable:false })
  plan: PlanOutput;
	
	@Field(() => PaymentGatewayOutput, { nullable:false })
  paymentGateway: PaymentGatewayOutput;

	@Field(() => Date, { nullable:false })
	createdAt: Date;

	@Field(() => Date, { nullable:false })
	updatedAt: Date;

	@Field(() => Date, { nullable:true })
	deletedAt: Date;
}