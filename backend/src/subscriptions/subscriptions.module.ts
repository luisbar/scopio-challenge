import { Module } from '@nestjs/common';
import { Subscription } from './subscription.model';
import { SubscriptionsService } from './subscriptions.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { SubscriptionsResolver } from './subscriptions.resolver';
import { Customer } from '../customers/customer.model';
import { CommonModule } from 'src/common/common.module';

@Module({
  imports: [SequelizeModule.forFeature([Subscription]), SequelizeModule.forFeature([Customer]), CommonModule],
  providers: [SubscriptionsService, SubscriptionsResolver]
})
export class SubscriptionsModule {}
