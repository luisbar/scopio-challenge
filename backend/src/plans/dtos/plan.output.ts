import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class PlanOutput {
	@Field(() => String, { nullable:false })
	id: string;

	@Field(() => String, { nullable:false })
	name: string;
	
	@Field(() => Number, { nullable:false })
  billingCycle: number;

	@Field(() => Number, { nullable:false })
	price: number;

	@Field(() => Date, { nullable:false })
	createdAt: Date;

	@Field(() => Date, { nullable:false })
	updatedAt: Date;

	@Field(() => Date, { nullable:true })
	deletedAt: Date;
}