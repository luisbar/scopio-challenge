import { Field, InputType } from '@nestjs/graphql';
import { IsString, IsNumber, Matches } from 'class-validator';

@InputType()
export class PlanInput {
    @Field(() => String, { nullable: false })
    @IsString({ message: 'Name is not a valid string' })
    @Matches(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/, {
        message: 'Name does not match the required format',
    })
    name: string;

    @Field(() => Number, { nullable: false })
    @IsNumber({}, { message: 'BillingCycle is not a valid number' })
    billingCycle: number;

    @Field(() => Number, { nullable: false })
    @IsNumber({}, { message: 'Price is not a valid number' })
    price: number;
}
