import { Module } from '@nestjs/common';
import { PlansResolver } from './plans.resolver';
import { SequelizeModule } from '@nestjs/sequelize';
import { Plan } from './plan.model';
import { PlansService } from './plans.service';
import { CommonModule } from 'src/common/common.module';

@Module({
  imports: [SequelizeModule.forFeature([Plan]), CommonModule],
  providers: [PlansResolver, PlansService]
})
export class PlansModule {}
