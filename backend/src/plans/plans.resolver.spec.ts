import { Test, TestingModule } from '@nestjs/testing';
import { PlansResolver } from './plans.resolver';
import { PlansService } from './plans.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { Plan } from './plan.model';
import { ConfigModule } from '@nestjs/config';
import { Subscription } from '../subscriptions/subscription.model';
import { Customer } from '../customers/customer.model';
import { PaymentGateway } from '../paymentGateways/paymentGateway.model';
import { CommonModule } from '../common/common.module';
let planId;

describe('PlansResolver', () => {
  let resolver: PlansResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        SequelizeModule.forRootAsync({
          useFactory: () => ({
            dialect: 'postgres',
            host: process.env.DATABASE_HOST,
            port: Number(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
            synchronize: process.env.NODE_ENV !== 'production',
            autoLoadModels: process.env.NODE_ENV !== 'production',
            models: [Plan, Subscription, Customer, PaymentGateway],
          })
        }),
        SequelizeModule.forFeature([Plan]),
        ConfigModule.forRoot({
          ignoreEnvFile: Boolean(process.env.IGNORE_ENV_FILE),
          isGlobal: true,
        }),
        CommonModule,
      ],
      providers: [PlansResolver, PlansService],
    }).compile();

    resolver = module.get<PlansResolver>(PlansResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('should add a plan', async () => {
    const plan = await resolver.createPlan({
      name: 'plan1',
      billingCycle: 1,
      price: 100,
    });
    planId = plan.id;

    expect(plan).toMatchObject({
      name: 'plan1',
      billingCycle: 1,
      price: 100,
    });
  });

  it('should find a plan', async () => {
    const plan = await resolver.findPlan(planId);

    expect(plan).toMatchObject({
      name: 'plan1',
      billingCycle: 1,
      price: 100,
    });
  });

  it('should return an array of plans', async () => {
    const plans = await resolver.findPlans();
    expect(plans.length).toBeGreaterThan(0);
  });

  it('should update a plan', async () => {
    const plan = await resolver.updatePlan(planId, {
      name: 'plan1',
      billingCycle: 2,
      price: 200,
    });

    expect(plan).toMatchObject({
      name: 'plan1',
      billingCycle: 2,
      price: 200,
    });
  });

  it('should delete a plan', async () => {
    const plan = await resolver.deletePlan(planId);

    expect(plan).toMatchObject({
      name: 'plan1',
      billingCycle: 2,
      price: 200,
    });
  });
});
