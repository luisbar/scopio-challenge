import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Plan } from './plan.model';

@Injectable()
export class PlansService {
  constructor(
    @InjectModel(Plan)
    private planModel: typeof Plan,
  ) {}

  findAll(): Promise<Plan[]> {
    return this.planModel.findAll();
  }

  findOne(id: string): Promise<Plan> {
    return this.planModel.findByPk(id);
  }

  create(plan: Plan): Promise<Plan> {
    const { name, billingCycle, price, } = plan;
    return this.planModel.create({
      name,
      billingCycle,
      price,
    });
  }

  async update(id: string, plan: Plan): Promise<Plan> {
    const { name, billingCycle, price, } = plan;
    await this.planModel.update({
      name,
      billingCycle,
      price,
    }, { where: { id } });

    return this.planModel.findByPk(id)
  }

  async delete(id: string): Promise<any> {
    const planToDelete = (await this.planModel.findByPk(id)).toJSON();
    await this.planModel.destroy({ where: { id } });

    return planToDelete;
  }
}
