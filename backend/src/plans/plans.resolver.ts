import { Mutation, Resolver, Query, Args } from '@nestjs/graphql';
import { Plan } from './plan.model';
import { PlansService } from './plans.service';
import { PlanInput } from './dtos/plan.input';
import { PlanOutput } from './dtos/plan.output';
import { LoggerService } from '../common/logger/logger.service';

@Resolver(of => PlanOutput)
export class PlansResolver {
  constructor(
    private plansService: PlansService,
    private readonly logger: LoggerService,
  ) {
    this.logger.setContext(PlansResolver.name);
  }

  @Query(returns => [PlanOutput])
  findPlans(
  ) {
    try {
      return this.plansService.findAll();
    } catch (error) {
      throw this.logger.error(
        'Cannot get plans',
        error,
      );
    }
  }

  @Query(returns => PlanOutput)
  findPlan(
    @Args('id') id: string,
  ) {
    try {
      return this.plansService.findOne(id);
    } catch (error) {
      throw this.logger.error(
        'Cannot get the plan',
        error,
      );
    }
  }

  @Mutation(returns => PlanOutput)
  createPlan(
    @Args('plan', { type: () => PlanInput }) plan: PlanInput,
  ) {
    try {
      return this.plansService.create(plan as Plan);
    } catch (error) {
      throw this.logger.error(
        'Cannot create the plan',
        error,
      );
    }
  }

  @Mutation(returns => PlanOutput)
  updatePlan(
    @Args('id') id: string,
    @Args('plan', { type: () => PlanInput }) plan: PlanInput,
  ) {
    try {
      return this.plansService.update(id, plan as Plan);
    } catch (error) {
      throw this.logger.error(
        'Cannot update the plan',
        error,
      );
    }
  }

  @Mutation(returns => PlanOutput)
  deletePlan(
    @Args('id') id: string,
  ) {
    try {
      return this.plansService.delete(id);
    } catch (error) {
      throw this.logger.error(
        'Cannot delete the plan',
        error,
      );
    }
  }
}