import { Test, TestingModule } from '@nestjs/testing';
import { PlansService } from './plans.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { Plan } from './plan.model';
import { ConfigModule } from '@nestjs/config';
import { Subscription } from '../subscriptions/subscription.model';
import { Customer } from '../customers/customer.model';
import { PaymentGateway } from '../paymentGateways/paymentGateway.model';
let planId;

describe('PlansService', () => {
  let service: PlansService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        SequelizeModule.forRootAsync({
          useFactory: () => ({
            dialect: 'postgres',
            host: process.env.DATABASE_HOST,
            port: Number(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
            synchronize: process.env.NODE_ENV !== 'production',
            autoLoadModels: process.env.NODE_ENV !== 'production',
            models: [Plan, Subscription, Customer, PaymentGateway],
          })
        }),
        SequelizeModule.forFeature([Plan]),
        ConfigModule.forRoot({
          ignoreEnvFile: Boolean(process.env.IGNORE_ENV_FILE),
          isGlobal: true,
        }),
      ],
      providers: [
        PlansService
      ],
    }).compile();

    service = module.get<PlansService>(PlansService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a plan', async () => {
    const plan = await service.create({
      name: 'plan1',
      billingCycle: 1,
      price: 100,
    } as Plan);
    planId = plan.id;

    expect(plan.toJSON()).toMatchObject({
      name: 'plan1',
      billingCycle: 1,
      price: 100,
    });
  });

  it('should find a plan by id', async () => {
    const plan = await service.findOne(planId);

    expect(plan.toJSON()).toMatchObject({
      name: 'plan1',
      billingCycle: 1,
      price: 100,
    });
  });

  it('should get all plans', async () => {
    const plans = await service.findAll();
    expect(plans.length).toBeGreaterThan(0);
  });

  it('should update a plan', async () => {
    const plan = await service.update(planId, {
      name: 'plan1',
      billingCycle: 2,
      price: 200,
    } as Plan);
    
    expect(plan.toJSON()).toMatchObject({
      name: 'plan1',
      billingCycle: 2,
      price: 200,
    });
  });

  it('should delete a plan', async () => {
    const plan = await service.delete(planId);

    expect(plan).toMatchObject({
      name: 'plan1',
      billingCycle: 2,
      price: 200,
    });
  });
});
