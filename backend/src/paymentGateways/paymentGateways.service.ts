import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { PaymentGateway } from './paymentGateway.model';

@Injectable()
export class PaymentGatewaysService {
  constructor(
    @InjectModel(PaymentGateway)
    private paymentGatewayModel: typeof PaymentGateway,
  ) {}

  findAll(): Promise<PaymentGateway[]> {
    return this.paymentGatewayModel.findAll();
  }

  findOne(id: string): Promise<PaymentGateway> {
    return this.paymentGatewayModel.findByPk(id);
  }

  create(paymentGateway: PaymentGateway): Promise<PaymentGateway> {
    const { name } = paymentGateway;
    return this.paymentGatewayModel.create({
      name,
    });
  }

  async update(id: string, paymentGateway: PaymentGateway): Promise<PaymentGateway> {
    const { name } = paymentGateway;
    await this.paymentGatewayModel.update({
      name,
    }, { where: { id } });

    return this.paymentGatewayModel.findByPk(id)
  }

  async delete(id: string): Promise<any> {
    const paymentGatewayToDelete = (await this.paymentGatewayModel.findByPk(id)).toJSON();
    await this.paymentGatewayModel.destroy({ where: { id } });

    return paymentGatewayToDelete;
  }
}
