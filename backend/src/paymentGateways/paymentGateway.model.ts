import { Column, Model, Table, HasMany, CreatedAt, UpdatedAt, DeletedAt } from 'sequelize-typescript';
import { DataTypes } from 'sequelize';
import { Subscription } from '../subscriptions/subscription.model';

@Table
export class PaymentGateway extends Model {
  @Column({
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  })
  id: string;

  @Column
  name: string;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @DeletedAt
  deletedAt: Date;

  @HasMany(() => Subscription)
  subscriptions: Subscription[]
}