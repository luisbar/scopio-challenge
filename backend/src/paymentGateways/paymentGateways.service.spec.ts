import { Test, TestingModule } from '@nestjs/testing';
import { PaymentGatewaysService } from './paymentGateways.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { PaymentGateway } from './paymentGateway.model';
import { ConfigModule } from '@nestjs/config';
import { Plan } from '../plans/plan.model';
import { Customer } from '../customers/customer.model';
import { Subscription } from '../subscriptions/subscription.model';
let paymentGatewayId;

describe('PaymentGatewaysService', () => {
  let service: PaymentGatewaysService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        SequelizeModule.forRootAsync({
          useFactory: () => ({
            dialect: 'postgres',
            host: process.env.DATABASE_HOST,
            port: Number(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
            synchronize: process.env.NODE_ENV !== 'production',
            autoLoadModels: process.env.NODE_ENV !== 'production',
            models: [PaymentGateway, Plan, Customer, Subscription],
          })
        }),
        SequelizeModule.forFeature([PaymentGateway]),
        ConfigModule.forRoot({
          ignoreEnvFile: Boolean(process.env.IGNORE_ENV_FILE),
          isGlobal: true,
        }),
      ],
      providers: [
        PaymentGatewaysService
      ],
    }).compile();

    service = module.get<PaymentGatewaysService>(PaymentGatewaysService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a paymentGateway', async () => {
    const paymentGateway = await service.create({
      name: 'paymentGateway1',
    } as PaymentGateway);
    paymentGatewayId = paymentGateway.id;

    expect(paymentGateway.toJSON()).toMatchObject({
      name: 'paymentGateway1',
    });
  });

  it('should find a paymentGateway by id', async () => {
    const paymentGateway = await service.findOne(paymentGatewayId);

    expect(paymentGateway.toJSON()).toMatchObject({
      name: 'paymentGateway1',
    });
  });

  it('should get all paymentGateways', async () => {
    const paymentGateways = await service.findAll();
    expect(paymentGateways.length).toBeGreaterThan(0);
  });

  it('should update a paymentGateway', async () => {
    const paymentGateway = await service.update(paymentGatewayId, {
      name: 'paymentGateway1',
    } as PaymentGateway);
    
    expect(paymentGateway.toJSON()).toMatchObject({
      name: 'paymentGateway1',
    });
  });

  it('should delete a paymentGateway', async () => {
    const paymentGateway = await service.delete(paymentGatewayId);

    expect(paymentGateway).toMatchObject({
      name: 'paymentGateway1',
    });
  });
});
