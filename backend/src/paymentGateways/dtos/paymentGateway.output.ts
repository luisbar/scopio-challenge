import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class PaymentGatewayOutput {
	@Field(() => String, { nullable:false })
	id: string;

	@Field(() => String, { nullable:false })
	name: string;

	@Field(() => Date, { nullable:false })
	createdAt: Date;

	@Field(() => Date, { nullable:false })
	updatedAt: Date;

	@Field(() => Date, { nullable:true })
	deletedAt: Date;
}