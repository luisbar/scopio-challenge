import { Field, InputType } from '@nestjs/graphql';
import { IsString, Matches } from 'class-validator';

@InputType()
export class PaymentGatewayInput {
    @Field(() => String, { nullable: false })
    @IsString({ message: 'Name is not a valid string' })
    @Matches(/^[a-zA-Z ,.ñÑ\u00C0-\u017F]*$/, {
        message: 'Name does not match the required format',
    })
    name: string;
}
