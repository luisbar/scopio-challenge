import { Module } from '@nestjs/common';
import { PaymentGatewaysResolver } from './paymentGateways.resolver';
import { PaymentGatewaysService } from './paymentGateways.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { PaymentGateway } from './paymentGateway.model';
import { CommonModule } from 'src/common/common.module';

@Module({
  imports: [SequelizeModule.forFeature([PaymentGateway]), CommonModule],
  providers: [PaymentGatewaysResolver, PaymentGatewaysService]
})
export class PaymentGatewaysModule {}
