import { Test, TestingModule } from '@nestjs/testing';
import { PaymentGatewaysResolver } from './paymentGateways.resolver';
import { PaymentGatewaysService } from './paymentGateways.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { PaymentGateway } from './paymentGateway.model';
import { ConfigModule } from '@nestjs/config';
import { Plan } from '../plans/plan.model';
import { Customer } from '../customers/customer.model';
import { Subscription } from '../subscriptions/subscription.model';
import { CommonModule } from '../common/common.module';
let paymentGatewayId;

describe('PaymentGatewaysResolver', () => {
  let resolver: PaymentGatewaysResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        SequelizeModule.forRootAsync({
          useFactory: () => ({
            dialect: 'postgres',
            host: process.env.DATABASE_HOST,
            port: Number(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
            synchronize: process.env.NODE_ENV !== 'production',
            autoLoadModels: process.env.NODE_ENV !== 'production',
            models: [PaymentGateway, Plan, Customer, Subscription],
          })
        }),
        SequelizeModule.forFeature([PaymentGateway]),
        ConfigModule.forRoot({
          ignoreEnvFile: Boolean(process.env.IGNORE_ENV_FILE),
          isGlobal: true,
        }),
        CommonModule,
      ],
      providers: [PaymentGatewaysResolver, PaymentGatewaysService],
    }).compile();

    resolver = module.get<PaymentGatewaysResolver>(PaymentGatewaysResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('should add a paymentGateway', async () => {
    const paymentGateway = await resolver.createPaymentGateway({
      name: 'paymentGateway1',
    });
    paymentGatewayId = paymentGateway.id;

    expect(paymentGateway).toMatchObject({
      name: 'paymentGateway1',
    });
  });

  it('should find a paymentGateway', async () => {
    const paymentGateway = await resolver.findPaymentGateway(paymentGatewayId);

    expect(paymentGateway).toMatchObject({
      name: 'paymentGateway1',
    });
  });

  it('should return an array of paymentGateways', async () => {
    const paymentGateways = await resolver.findPaymentGateways();
    expect(paymentGateways.length).toBeGreaterThan(0);
  });

  it('should update a paymentGateway', async () => {
    const paymentGateway = await resolver.updatePaymentGateway(paymentGatewayId, {
      name: 'paymentGateway1',
    });

    expect(paymentGateway).toMatchObject({
      name: 'paymentGateway1',
    });
  });

  it('should delete a paymentGateway', async () => {
    const paymentGateway = await resolver.deletePaymentGateway(paymentGatewayId);

    expect(paymentGateway).toMatchObject({
      name: 'paymentGateway1',
    });
  });
});
