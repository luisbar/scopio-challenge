import { Mutation, Resolver, Query, Args } from '@nestjs/graphql';
import { PaymentGateway } from './paymentGateway.model';
import { PaymentGatewaysService } from './paymentGateways.service';
import { PaymentGatewayInput } from './dtos/paymentGateway.input';
import { PaymentGatewayOutput } from './dtos/paymentGateway.output';
import { LoggerService } from '../common/logger/logger.service';

@Resolver(of => PaymentGatewayOutput)
export class PaymentGatewaysResolver {
  constructor(
    private paymentGatewaysService: PaymentGatewaysService,
    private readonly logger: LoggerService,
  ) {
    this.logger.setContext(PaymentGatewaysResolver.name);
  }

  @Query(returns => [PaymentGatewayOutput])
  findPaymentGateways(
  ) {
    try {
      return this.paymentGatewaysService.findAll();
    } catch (error) {
      throw this.logger.error(
        'Cannot get payment gateways',
        error,
      );
    }
  }

  @Query(returns => PaymentGatewayOutput)
  findPaymentGateway(
    @Args('id') id: string,
  ) {
    try {
      return this.paymentGatewaysService.findOne(id);
    } catch (error) {
      throw this.logger.error(
        'Cannot get the payment gateway',
        error,
      );
    }
  }

  @Mutation(returns => PaymentGatewayOutput)
  createPaymentGateway(
    @Args('paymentGateway', { type: () => PaymentGatewayInput }) paymentGateway: PaymentGatewayInput,
  ) {
    try {
      return this.paymentGatewaysService.create(paymentGateway as PaymentGateway);
    } catch (error) {
      throw this.logger.error(
        'Cannot create the payment gateway',
        error,
      );
    }
  }

  @Mutation(returns => PaymentGatewayOutput)
  updatePaymentGateway(
    @Args('id') id: string,
    @Args('paymentGateway', { type: () => PaymentGatewayInput }) paymentGateway: PaymentGatewayInput,
  ) {
    try {
      return this.paymentGatewaysService.update(id, paymentGateway as PaymentGateway);
    } catch (error) {
      throw this.logger.error(
        'Cannot update the payment gateway',
        error,
      );
    }
  }

  @Mutation(returns => PaymentGatewayOutput)
  deletePaymentGateway(
    @Args('id') id: string,
  ) {
    try {
      return this.paymentGatewaysService.delete(id);
    } catch (error) {
      throw this.logger.error(
        'Cannot delete the payment gateway',
        error,
      );
    }
  }
}