import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LoggerService } from './common/logger/logger.service';

const setupLogger = (app) => {
  app.useLogger(new LoggerService().setContext('Default'));
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });
  setupLogger(app);
  await app.listen(3000);
}

bootstrap();
