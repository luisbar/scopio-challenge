import { Module } from '@nestjs/common';
import { PlansModule } from './plans/plans.module';
import { SubscriptionsModule } from './subscriptions/subscriptions.module';
import { PaymentGatewaysModule } from './paymentGateways/paymentGateways.module';
import { CustomersModule } from './customers/customers.module';
import { SequelizeModule } from '@nestjs/sequelize';
import { Customer } from './customers/customer.model';
import { GraphQLModule } from '@nestjs/graphql';
import * as path from 'path';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ConfigModule } from '@nestjs/config';
import { Plan } from './plans/plan.model';
import { PaymentGateway } from './paymentGateways/paymentGateway.model';
import { Subscription } from './subscriptions/subscription.model';
import { APP_FILTER, APP_PIPE } from '@nestjs/core';
import { BodyValidationPipe } from './common/validation/body.validation.pipe';
import { CommonModule } from './common/common.module';
import { GenericExceptionFilter } from './common/exception/generic.exception.filter';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      cors: true,
      introspection: process.env.NODE_ENV !== 'production',
      autoTransformHttpErrors: false,
      debug: process.env.NODE_ENV !== 'production',
      playground: process.env.NODE_ENV !== 'production',
      path: '/api/graphql',
      autoSchemaFile: path.join(process.cwd(), './schema.gql'),
      context: ({ req, res }) => {
        return {};
      },
    }),
    SequelizeModule.forRootAsync({
      useFactory: () => ({
        dialect: 'postgres',
        host: process.env.DATABASE_HOST,
        port: Number(process.env.DATABASE_PORT),
        username: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_NAME,
        synchronize: process.env.NODE_ENV !== 'production',
        autoLoadModels: process.env.NODE_ENV !== 'production',
        models: [
          Customer,
          Plan,
          PaymentGateway,
          Subscription
        ],
      })
    }),
    ConfigModule.forRoot({
      ignoreEnvFile: Boolean(process.env.IGNORE_ENV_FILE),
      isGlobal: true,
    }),
    PlansModule,
    SubscriptionsModule,
    PaymentGatewaysModule,
    CustomersModule,
    CommonModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_PIPE,
      useClass: BodyValidationPipe,
    },
    {
      provide: APP_FILTER,
      useClass: GenericExceptionFilter,
    },
  ],
})
export class AppModule {}
