import { Plugin } from '@nestjs/apollo';
import {
  ApolloServerPlugin,
  BaseContext,
  GraphQLRequestContextWillSendResponse,
  GraphQLRequestListener,
} from 'apollo-server-plugin-base';
import * as ramda from 'ramda';

@Plugin()
export class ErrorFormatter implements ApolloServerPlugin {

  async requestDidStart(): Promise<GraphQLRequestListener> {
    return {
      async willSendResponse(requestContext: GraphQLRequestContextWillSendResponse<BaseContext>): Promise<void> {
        if (!requestContext?.response?.errors ||
            requestContext?.response?.errors[0].extensions?.exception['stacktrace'][0].includes('GraphQLError')) return;
        
        const rest = ramda.omit(['response', 'name', 'stacktrace'], requestContext?.response?.errors[0]?.extensions.exception);

        requestContext.response.http.status = rest.status;
        requestContext.response.errors = [
          {
            ...rest,
          },
        ];
        
      },
    };
  }
}