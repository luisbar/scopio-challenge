import { TraceException } from '../exception/trace.exception';
const pino = require('pino');
const logger = pino({
  prettyPrint: {
    colorize: true,
    levelFirst: true,
    hideObject: true,
    messageFormat: '{context}:{message}',
    translateTime: 'mm/dd/yyyy, h:MM:ss TT',
  },
  level: process.env.NODE_ENV !== 'production' ? 'trace' : 'error',
});

export class LoggerService {

  private context: string;

  log(message: any) {
   logger.info({
      message,
      context: this.context,
    });
  }

  error(message: any, error: Error): Error {
    const traceError = new TraceException(message, error);
    logger.error({ message: traceError.messages().join(':'), context: this.context, });
    return traceError;
  }

  warn(message: any) {
    logger.warn({
      message,
      context: this.context,
    });
  }

  debug(message: any) {
    logger.debug({
      message,
      context: this.context,
    });
  }

  setContext(context: string) {
    this.context = context;
    return this;
  }
}