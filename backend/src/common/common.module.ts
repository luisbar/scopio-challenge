import { Module } from '@nestjs/common';
import { ErrorFormatter } from './apollo/error.formatter';
import { LoggerService } from './logger/logger.service';

@Module({
  providers: [
    ErrorFormatter,
    LoggerService,
  ],
  exports: [
    LoggerService,
  ]
})
export class CommonModule {}
