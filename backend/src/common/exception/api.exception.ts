import { HttpException } from '@nestjs/common';

export class ApiException extends HttpException {

  httpErrorMessage: string;
  fields: object;
  messages: string[];

  constructor(httpErrorMessage: string, httpStatus: number, message: string, fields: object, messages?: string[]) {
    super(message, httpStatus);
    this.httpErrorMessage = httpErrorMessage;
    this.fields = fields;
    this.messages = messages;
  }
}