import {
  Catch,
  ArgumentsHost,
  HttpStatus,
} from '@nestjs/common';
import * as ramda from 'ramda';
import { KeyValuePair } from 'ramda';
import { TraceException } from './trace.exception';
import { GqlArgumentsHost, GqlExceptionFilter } from '@nestjs/graphql';
import { ApiException } from './api.exception';

@Catch()
export class GenericExceptionFilter implements GqlExceptionFilter {

  getHttpStatusCode(exception: Error) {
    return ramda.pathOr(HttpStatus.INTERNAL_SERVER_ERROR, ['status'], exception);
  }

  getHttpErrorMessage(exception: Error) {
    return ramda.pathOr('Internal server error', ['httpErrorMessage'], exception);
  }

  getMessage(exception: Error, lang: string) {
    return ramda.pathOr(
      'Something went wrong',
      ['message'],
      exception
    )
  }

  async getFields(exception: Error, lang: string) {
    return ramda
    .fromPairs(
      ramda
      .splitEvery(
        2,
        await Promise.all(
          ramda
          .compose(
            ramda.once(ramda.flatten),
            ramda.map(([key, value]: string[]) => [key, value],),
            ramda.toPairs,
            ramda.pathOr({}, ['fields']),
          )
          (exception)
        )
      ) as KeyValuePair<string, any>[]
    );
  }

  async catch(exception: Error, host: ArgumentsHost) {
    const gqlHost = GqlArgumentsHost.create(host);
    
    const context = gqlHost.getContext();
    const request = context.req;
    const language = ramda.pathOr('en', ['headers', 'accept-language'], request);
    
    const httpStatusCode = this.getHttpStatusCode(exception);
    const httpErrorMessage = this.getHttpErrorMessage(exception);
    const message = await this.getMessage(exception, language);
    const fields = await this.getFields(exception, language);
    const messages = exception.constructor.name === 'TraceException' ? (exception as TraceException).messages() : [];
    
    return new ApiException(httpErrorMessage, httpStatusCode, message, fields, messages);
  }
}