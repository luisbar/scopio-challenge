const TraceError = require('trace-error');
import { ApiException } from './api.exception';

export class TraceException extends TraceError {
  
  status: number;
  httpErrorMessage: string;
  name: string;
  message: string;

  constructor(message: string, error: Error) {
    super(message, error);
    if (error instanceof ApiException) {
      const apiException = error as ApiException;
      this.status = apiException.getStatus();
      this.httpErrorMessage = apiException.httpErrorMessage;
    }
  }
}