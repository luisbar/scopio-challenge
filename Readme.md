## How to run?

1. Install docker
2. Execute `docker-compose up -d`
3. You can open the GraphQL playground clicking the following url: http://localhost:3003/api/graphql
4. You can open the Frontend clicking the following url: http://localhost:3002