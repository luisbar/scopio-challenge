--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Customers; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public."Customers" (
    id character varying(255) NOT NULL,
    "firstName" character varying(255),
    "lastName" character varying(255),
    role character varying(255),
    email character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);


ALTER TABLE public."Customers" OWNER TO root;

--
-- Name: PaymentGateways; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public."PaymentGateways" (
    id character varying(255) NOT NULL,
    name character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);


ALTER TABLE public."PaymentGateways" OWNER TO root;

--
-- Name: Plans; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public."Plans" (
    id character varying(255) NOT NULL,
    name character varying(255),
    "billingCycle" integer,
    price integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);


ALTER TABLE public."Plans" OWNER TO root;

--
-- Name: Subscriptions; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public."Subscriptions" (
    id character varying(255) NOT NULL,
    "endsAt" timestamp with time zone,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "planId" character varying(255),
    "customerId" character varying(255),
    "paymentGatewayId" character varying(255)
);


ALTER TABLE public."Subscriptions" OWNER TO root;

--
-- Data for Name: Customers; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public."Customers" (id, "firstName", "lastName", role, email, "createdAt", "updatedAt", "deletedAt") FROM stdin;
\.


--
-- Data for Name: PaymentGateways; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public."PaymentGateways" (id, name, "createdAt", "updatedAt", "deletedAt") FROM stdin;
b1427da3-c54c-4820-98d7-620a74a0a97e	Stripe	2022-04-25 19:20:29.765+00	2022-04-25 19:20:29.765+00	\N
934fb274-1b97-4e8c-8446-0d796333d832	Paypal	2022-04-25 19:20:35.074+00	2022-04-25 19:20:35.074+00	\N
\.


--
-- Data for Name: Plans; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public."Plans" (id, name, "billingCycle", price, "createdAt", "updatedAt", "deletedAt") FROM stdin;
4f527132-6f85-4bb8-aae6-f0a4d284d6bf	The creative	4	1000	2022-04-25 19:19:19.161+00	2022-04-25 19:19:19.161+00	\N
844d9700-25d9-4bae-8faf-cfc2e80f33fb	Adventurer	4	1000	2022-04-25 19:20:02.806+00	2022-04-25 19:20:02.806+00	\N
f1b17a7a-2d21-44e8-b6bf-424920610a10	Super start	4	1000	2022-04-25 19:20:07.738+00	2022-04-25 19:20:07.738+00	\N
34e24481-25d2-4799-ba2d-9efccef5f949	Infinity	4	1000	2022-04-25 19:20:14.42+00	2022-04-25 19:20:14.42+00	\N
\.


--
-- Data for Name: Subscriptions; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public."Subscriptions" (id, "endsAt", "createdAt", "updatedAt", "deletedAt", "planId", "customerId", "paymentGatewayId") FROM stdin;
\.


--
-- Name: Customers Customers_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."Customers"
    ADD CONSTRAINT "Customers_pkey" PRIMARY KEY (id);


--
-- Name: PaymentGateways PaymentGateways_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."PaymentGateways"
    ADD CONSTRAINT "PaymentGateways_pkey" PRIMARY KEY (id);


--
-- Name: Plans Plans_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."Plans"
    ADD CONSTRAINT "Plans_pkey" PRIMARY KEY (id);


--
-- Name: Subscriptions Subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."Subscriptions"
    ADD CONSTRAINT "Subscriptions_pkey" PRIMARY KEY (id);


--
-- Name: Subscriptions Subscriptions_customerId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."Subscriptions"
    ADD CONSTRAINT "Subscriptions_customerId_fkey" FOREIGN KEY ("customerId") REFERENCES public."Customers"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Subscriptions Subscriptions_paymentGatewayId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."Subscriptions"
    ADD CONSTRAINT "Subscriptions_paymentGatewayId_fkey" FOREIGN KEY ("paymentGatewayId") REFERENCES public."PaymentGateways"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Subscriptions Subscriptions_planId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."Subscriptions"
    ADD CONSTRAINT "Subscriptions_planId_fkey" FOREIGN KEY ("planId") REFERENCES public."Plans"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--